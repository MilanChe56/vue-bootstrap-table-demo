import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource'
import {ServerTable, ClientTable, Event} from 'vue-tables-2';

Vue.use(VueResource);
Vue.use(ClientTable, {}, false, 'bootstrap4', 'default');
Vue.use(ServerTable, {}, false, 'bootstrap4', 'default');


new Vue({
  el: '#app',
  resource: VueResource,
  render: h => h(App)
})
