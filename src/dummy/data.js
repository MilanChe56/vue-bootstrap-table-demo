export const _tableData = [{
  "_id": 0,
  "Time interval": 1383264000000,
  "SMS-in activity": 1.0,
  "SMS-out activity": null,
  "Call-in activity": null,
  "Call-out activity": null,
  "Internet traffic activity": null,
  "Imsi": "10052340009271",
  "Base_station": "KG3060",
  "LONGITUDE": 19.8299694444,
  "LATITUDE": 43.8600444444,
  "Site_name": "KG3060",
  "District": "ZLATIBORSKI OKRUG",
  "Municipality": "Uzice",
  "Town": "Uzice",
  "Place": "Uzice",
  "Technology": "2G"
}, {
  "_id": 1,
  "Time interval": 1383264000000,
  "SMS-in activity": 1.0,
  "SMS-out activity": 1.0,
  "Call-in activity": 1.0,
  "Call-out activity": 1.0,
  "Internet traffic activity": 14.5486740483,
  "Imsi": "10052130600310",
  "Base_station": "BG0524",
  "LONGITUDE": 20.201839,
  "LATITUDE": 44.778944,
  "Site_name": "BG0524",
  "District": "GRAD BEOGRAD",
  "Municipality": "Beograd - Surcin",
  "Town": "Beograd",
  "Place": "Becmen",
  "Technology": "2G"
}, {
  "_id": 2,
  "Time interval": 1383264000000,
  "SMS-in activity": null,
  "SMS-out activity": 1.0,
  "Call-in activity": null,
  "Call-out activity": null,
  "Internet traffic activity": null,
  "Imsi": "10052130600310",
  "Base_station": "BG0524",
  "LONGITUDE": 20.201839,
  "LATITUDE": 44.778944,
  "Site_name": "BG0524",
  "District": "GRAD BEOGRAD",
  "Municipality": "Beograd - Surcin",
  "Town": "Beograd",
  "Place": "Becmen",
  "Technology": "2G"
}, {
  "_id": 3,
  "Time interval": 1383264000000,
  "SMS-in activity": 1.0,
  "SMS-out activity": 1.0,
  "Call-in activity": 1.0,
  "Call-out activity": 1.0,
  "Internet traffic activity": 19.3053470782,
  "Imsi": "10052300006461",
  "Base_station": "BG0477",
  "LONGITUDE": 20.381971,
  "LATITUDE": 44.843135,
  "Site_name": "BG0477",
  "District": "GRAD BEOGRAD",
  "Municipality": "Beograd - Zemun",
  "Town": "Beograd",
  "Place": "Zemun",
  "Technology": "3G"
}, {
  "_id": 4,
  "Time interval": 1383264000000,
  "SMS-in activity": 1.0,
  "SMS-out activity": 1.0,
  "Call-in activity": 1.0,
  "Call-out activity": 1.0,
  "Internet traffic activity": 23.2618275676,
  "Imsi": "10052350015197",
  "Base_station": "BG0511",
  "LONGITUDE": 20.196045,
  "LATITUDE": 44.877016,
  "Site_name": "BG0511",
  "District": "GRAD BEOGRAD",
  "Municipality": "Beograd - Zemun",
  "Town": "Beograd",
  "Place": "Ugrinovci",
  "Technology": "4G"
}];

export const tableData = [{
  code: "ZW",
  name: "Zimbabwe",
  created_at: "2015-04-24T01:46:50.459583",
  updated_at: "2015-04-24T01:46:50.459593",
  uri: "http://api.lobbyfacts.eu/api/1/country/245",
  id: 245
}, {
  code: "ZM",
  name: "Zambia",
  created_at: "2015-04-24T01:46:50.457459",
  updated_at: "2015-04-24T01:46:50.457468",
  uri: "http://api.lobbyfacts.eu/api/1/country/244",
  id: 244
}, {
  code: "YE",
  name: "Yemen",
  created_at: "2015-04-24T01:46:50.454731",
  updated_at: "2015-04-24T01:46:50.454741",
  uri: "http://api.lobbyfacts.eu/api/1/country/243",
  id: 243
}, {
  code: "EH",
  name: "Western Sahara",
  created_at: "2015-04-24T01:46:50.452002",
  updated_at: "2015-04-24T01:46:50.452011",
  uri: "http://api.lobbyfacts.eu/api/1/country/242",
  id: 242
}, {
  code: "ZW",
  name: "Zimbabwe",
  created_at: "2015-04-24T01:46:50.459583",
  updated_at: "2015-04-24T01:46:50.459593",
  uri: "http://api.lobbyfacts.eu/api/1/country/245",
  id: 245
}, {
  code: "ZM",
  name: "Zambia",
  created_at: "2015-04-24T01:46:50.457459",
  updated_at: "2015-04-24T01:46:50.457468",
  uri: "http://api.lobbyfacts.eu/api/1/country/244",
  id: 244
}, {
  code: "YE",
  name: "Yemen",
  created_at: "2015-04-24T01:46:50.454731",
  updated_at: "2015-04-24T01:46:50.454741",
  uri: "http://api.lobbyfacts.eu/api/1/country/243",
  id: 243
}, {
  code: "EH",
  name: "Western Sahara",
  created_at: "2015-04-24T01:46:50.452002",
  updated_at: "2015-04-24T01:46:50.452011",
  uri: "http://api.lobbyfacts.eu/api/1/country/242",
  id: 242
}, {
  code: "ZW",
  name: "Zimbabwe",
  created_at: "2015-04-24T01:46:50.459583",
  updated_at: "2015-04-24T01:46:50.459593",
  uri: "http://api.lobbyfacts.eu/api/1/country/245",
  id: 245
}, {
  code: "ZM",
  name: "Zambia",
  created_at: "2015-04-24T01:46:50.457459",
  updated_at: "2015-04-24T01:46:50.457468",
  uri: "http://api.lobbyfacts.eu/api/1/country/244",
  id: 244
}, {
  code: "YE",
  name: "Yemen",
  created_at: "2015-04-24T01:46:50.454731",
  updated_at: "2015-04-24T01:46:50.454741",
  uri: "http://api.lobbyfacts.eu/api/1/country/243",
  id: 243
}, {
  code: "EH",
  name: "Western Sahara",
  created_at: "2015-04-24T01:46:50.452002",
  updated_at: "2015-04-24T01:46:50.452011",
  uri: "http://api.lobbyfacts.eu/api/1/country/242",
  id: 242
}, {
  code: "ZW",
  name: "Zimbabwe",
  created_at: "2015-04-24T01:46:50.459583",
  updated_at: "2015-04-24T01:46:50.459593",
  uri: "http://api.lobbyfacts.eu/api/1/country/245",
  id: 245
}, {
  code: "ZM",
  name: "Zambia",
  created_at: "2015-04-24T01:46:50.457459",
  updated_at: "2015-04-24T01:46:50.457468",
  uri: "http://api.lobbyfacts.eu/api/1/country/244",
  id: 244
}, {
  code: "YE",
  name: "Yemen",
  created_at: "2015-04-24T01:46:50.454731",
  updated_at: "2015-04-24T01:46:50.454741",
  uri: "http://api.lobbyfacts.eu/api/1/country/243",
  id: 243
}, {
  code: "EH",
  name: "Western Sahara",
  created_at: "2015-04-24T01:46:50.452002",
  updated_at: "2015-04-24T01:46:50.452011",
  uri: "http://api.lobbyfacts.eu/api/1/country/242",
  id: 242
}, {
  code: "ZW",
  name: "Zimbabwe",
  created_at: "2015-04-24T01:46:50.459583",
  updated_at: "2015-04-24T01:46:50.459593",
  uri: "http://api.lobbyfacts.eu/api/1/country/245",
  id: 245
}, {
  code: "ZM",
  name: "Zambia",
  created_at: "2015-04-24T01:46:50.457459",
  updated_at: "2015-04-24T01:46:50.457468",
  uri: "http://api.lobbyfacts.eu/api/1/country/244",
  id: 244
}, {
  code: "YE",
  name: "Yemen",
  created_at: "2015-04-24T01:46:50.454731",
  updated_at: "2015-04-24T01:46:50.454741",
  uri: "http://api.lobbyfacts.eu/api/1/country/243",
  id: 243
}, {
  code: "EH",
  name: "Western Sahara",
  created_at: "2015-04-24T01:46:50.452002",
  updated_at: "2015-04-24T01:46:50.452011",
  uri: "http://api.lobbyfacts.eu/api/1/country/242",
  id: 242
}, {
  code: "ZW",
  name: "Zimbabwe",
  created_at: "2015-04-24T01:46:50.459583",
  updated_at: "2015-04-24T01:46:50.459593",
  uri: "http://api.lobbyfacts.eu/api/1/country/245",
  id: 245
}, {
  code: "ZM",
  name: "Zambia",
  created_at: "2015-04-24T01:46:50.457459",
  updated_at: "2015-04-24T01:46:50.457468",
  uri: "http://api.lobbyfacts.eu/api/1/country/244",
  id: 244
}, {
  code: "YE",
  name: "Yemen",
  created_at: "2015-04-24T01:46:50.454731",
  updated_at: "2015-04-24T01:46:50.454741",
  uri: "http://api.lobbyfacts.eu/api/1/country/243",
  id: 243
}, {
  code: "EH",
  name: "Western Sahara",
  created_at: "2015-04-24T01:46:50.452002",
  updated_at: "2015-04-24T01:46:50.452011",
  uri: "http://api.lobbyfacts.eu/api/1/country/242",
  id: 242
}];

export const countriesJsonUrl = 'https://api.myjson.com/bins/1bmr5z';
export const productsJsonUrl = 'https://api.myjson.com/bins/lm413';
